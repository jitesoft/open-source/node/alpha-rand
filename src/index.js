/**
 * Generate a random alphabetic string.
 * @param {number} size Character length of the string.
 * @return {string}
 */
module.exports = (size) => String.fromCharCode(...(require('crypto').randomBytes(size).map((i) => (Math.round(i % 25) + (i > 128 ? 65 : 97)))));
