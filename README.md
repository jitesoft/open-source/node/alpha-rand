# Random alphabetic string generator

Simple implementation of random string generation using crypto for better randomness.  
The string will consist of alphabetic characters only, both lower and upper case (a-zA-Z).

## Usage

Install the package:

```
npm install alpha-rand
yarn add alpha-rand
```

Import and use:

```
const strRandom = require('alpha-rand');
strRandom(128); // 128 characters random string.
strRandom(32); // 32 characters random string.
```

Or, even more easy: just copy the code!
